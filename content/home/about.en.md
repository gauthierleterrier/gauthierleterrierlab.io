+++
# About/Biography widget.
widget = "about"
active = true
date = 2016-04-20T00:00:00

# Order that this section will appear in.
weight = 3

# List your academic interests.
[interests]
  interests = [
    "Arithmetic Geometry"
  ]

# List your qualifications (such as academic degrees).
[[education.courses]]
  course = "M.A. in Mathematics"
  institution = "[EPFL](https://www.epfl.ch/)"
  year = 2018

[[education.courses]]
  course = "B.A. in Mathematics"
  institution = "[EPFL](https://www.epfl.ch/)"
  year = 2016

 
+++

## Welcome!

I am currently a PhD student at ...