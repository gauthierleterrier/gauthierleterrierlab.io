+++
# Custom widget.
# An example of using the custom widget to create your own homepage section.
# To create more sections, duplicate this file and edit the values below as desired.
widget = "custom"
active = true
date = 2017-04-20T00:00:00

# Note: a full width section format can be enabled by commenting out the `title` and `subtitle` with a `#`.
title = "Notes and projects"
subtitle = ""

# Order that this section will appear in.
weight = 20

+++

Here are some of personal notes about various topics.

<div class="row">
	<h3>2018</h3>
</div>

[Poster for my Master's project](files/projects/Poster_Projet_Master_EPFL_Gauthier_Leterrier_Special_cycles_on_unitary_Shimura_varieties.pdf), about unitary Shimura varieties.