+++
# Biographie widget.
widget = "about"
active = true
date = 2016-04-20T00:00:00

# Order that this section will appear in.
weight = 5

# List your academic interests.
[interests]
  interests = [
    "Géométrie arithmétique",
    "Théorie des nombres"
  ]

# List your qualifications (such as academic degrees).
[[education.courses]]
  course = "Master en mathématiques théoriques"
  institution = "[EPFL](https://www.epfl.ch/)"
  year = 2018

[[education.courses]]
  course = "Bachelor en mathématiques"
  institution = "[EPFL](https://www.epfl.ch/)"
  year = 2016

+++

## Bienvenue !

Je suis candidat à l'école doctorale à ???