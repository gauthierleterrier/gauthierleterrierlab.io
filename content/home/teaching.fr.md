+++
# Custom widget.
# An example of using the custom widget to create your own homepage section.
# To create more sections, duplicate this file and edit the values below as desired.
widget = "custom"
active = true
date = 2016-04-20T00:00:00

# Note: a full width section format can be enabled by commenting out the `title` and `subtitle` with a `#`.
title = "Enseignement"
subtitle = ""

# Order that this section will appear in.
weight = 60


#write in html...   or create  folder .../content/teaching ??
+++

Depuis 2015, j'ai été assistant pour les cours suivants : théorie des groupes, théorie des anneaux et des corps, analyse IV.
