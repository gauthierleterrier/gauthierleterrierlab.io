+++
date = 2018-12-17
draft = false
tags = ["Automorphic forms", "Galois representations"]
title = "Short post to test reading time"
math = true
+++

SAGE's computations on Sato–Tate or BSD for abelian varieties...

Additionally, HTML may be used for advanced formatting.<!--more--> This article gives an overview of the most common formatting options.

{{% toc %}}

## Some experimental mathematics






$\newcommand{\Q}{\Bbb Q}
\newcommand{\N}{\Bbb N}
\newcommand{\R}{\Bbb R}
\newcommand{\Z}{\Bbb Z}
\newcommand{\C}{\Bbb C}
\newcommand{\F}{\Bbb F}
\newcommand{\p}{\mathfrak{p}}
$
Let $A$ be an abelian variety over a number field $F$.
It is expected that the $L$-function of $A$ has analytic continuation to $\Bbb C$ and satisfies a functional equation relating $s$ to $2-s$. 
In that setting, the (generalized) Birch–Swinnerton-Dyer conjecture states that

$$\mathrm{ord}\_{s=1}(L(A\_{/F},s)) = \mathrm{rk}_{\Z}(A(F)) =: r.$$


In fact, we conjecture that

$$\prod\_{N(\p) \leq x} L\_{\p}(A_{/F}, N(\p)^{-1}) \sim  C \log(x)^r,$$ 

where $L\_{\p}(A_{/F},s)$ is the local factor of the L-function of $A$ at $\p$.









<!-- Courses -->
<div class="row">
	<h3>Hobbies</h3>
	<p>
		Swimming  / Fishing  / Reading(History) <br />
	</p>		
	<h3>Marathon</h3>
	<p>
		<a href="https://gauthierleterrier.gitlab.io/img/bubbles-wide.jpg">Cool picture</a> <br />
	</p>
</div>