+++
date = 2016-04-17
draft = false
tags = ["Shimura varieties", "Galois representations"]
title = "Writing content with Markdown, LaTeX, and Shortcodes"
math = true
+++

Content can be written using [Markdown](https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet), [LaTeX math](https://en.wikibooks.org/wiki/LaTeX/Mathematics), and [Hugo Shortcodes](http://gohugo.io/extras/shortcodes/). Additionally, HTML may be used for advanced formatting.<!--more--> This article gives an overview of the most common formatting options.

{{% toc %}}

## Sub-headings

    ## Heading 2
    ### Heading 3
    #### Heading 4
    ##### Heading 5
    ###### Heading 6

## Emphasis

    Italics with *asterisks* or _underscores_.

    Bold with **asterisks** or __underscores__.

    Combined emphasis with **asterisks and _underscores_**.

    Strikethrough with ~~two tildes~~.

## Ordered lists

    1. First item
    2. Another item

## Unordered lists

    * First item
    * Another item

## Images

Images may be added to a page by placing them in your `static/img/` folder and referencing them using one of the following two notations:

A general image is added via the code:

    ![alternative text for search engines](img/bubbles-wide.jpg)

which should give ![alternative text for search engines](img/bubbles-wide.jpg).
but it doesn't work, while

    ![alternative text for search engines](https://gauthierleterrier.gitlab.io/website3/img/bubbles-wide.jpg)
gives ![alternative text for search engines](https://gauthierleterrier.gitlab.io/website3/img/bubbles-wide.jpg).


A numbered figure with caption:

    {{</* figure src="/img/screenshot.png" title="Figure Caption" */>}}

## Links

    [I'm a link](https://www.google.com)
    [A post]({{</* ref "post/hi.md" */>}})
    [A publication]({{</* ref "publication/hi.md" */>}})
    [A project]({{</* ref "project/hi.md" */>}})
    [Another section]({{</* relref "hi.md#who" */>}})
    
To enable linking to a file, such as a PDF, first place the file in your `static/files/` folder and then link to it using the following form:

    {{%/* staticref "files/cv.pdf" "newtab" */%}}Download my CV{{%/* /staticref */%}}

The optional `"newtab"` argument for `staticref` will cause the link to be opened in a new tab.

## Emojis

See the [Emoji cheat sheet](http://www.webpagefx.com/tools/emoji-cheat-sheet/) for available emoticons. The following serves as an example, but you should remove the spaces between each emoji name and pair of semicolons:

    I : heart : Academic : smile :
    
I :heart: Academic :smile:

## Blockquote

    > This is a blockquote.

> This is a blockquote.

## Footnotes

    I have more [^1] to say.
    
    [^1]: Footnote example.

I have more [^1] to say.
[^1]: Footnote example.

## Code highlighting

Pass the *language* of the code, such as `python`, as a parameter after three backticks:

    ```python
    # Example of code highlighting
    input_string_var = input("Enter some data: ")
    print("You entered: {}".format(input_string_var))
    ```
Result:

```python
# Example of code highlighting
input_string_var = input("Enter some data: ")
print("You entered: {}".format(input_string_var))
```

### Highlighting options

The Academic theme uses [highlight.js](https://highlightjs.org) for source code highlighting, and highlighting is enabled by default for all pages. However, several configuration options are supported that allow finer-grained control over highlight.js.

The following table lists the supported options for configuring highlight.js, along with their expected type and a short description. A "yes" in the **config.toml** column means the value can be set globally in `config.toml`, and a "yes" in the **preamble** column means that the value can be set locally in a particular page's preamble.

option                | type    | description                     | config.toml | preamble
----------------------|---------|---------------------------------|-------------|---------
`highlight`           | boolean | enable/disable highlighting     | yes         | yes
`highlight_languages` | slice   | choose additional languages     | yes         | yes
`highlight_style`     | string  | choose a highlighting style     | yes         | no


#### Option `highlight`

The `highlight` option allows enabling or disabling the inclusion of highlight.js, either globally or for a particular page. If the option is unset, it has the same effect as if you had specified `highlight = true`. That is, the highlight.js javascript and css files will be included in every page. If you'd like to only include highlight.js files on pages that actually require source code highlighting, you can set `highlight = false` in `config.toml`, and then override it by setting `highlight = true` in the preamble of any pages that require source code highlighting. Conversely, you could enable highlighting globally, and disable it locally for pages that do not require it. Here is a table that shows whether highlighting will be enabled for a page, based on the values of `highlight` set in `config.toml` and/or the page's preamble.

config.toml   | page preamble  | highlighting enabled for page?
--------------|----------------|-------------------------------
unset or true | unset or true  | yes
unset or true | false          | no
false         | unset or false | no
false         | true           | yes


## $\rm \LaTeX$ math

```TeX
$$\left [ – \frac{\hbar^2}{2 m} \frac{\partial^2}{\partial x^2} + V \right ] \Psi = i \hbar \frac{\partial}{\partial t} \Psi$$
```

$$\left [ – \frac{\hbar^2}{2 m} \frac{\partial^2}{\partial x^2} + V \right ] \Psi = i \hbar \frac{\partial}{\partial t} \Psi$$

Alternatively, inline math can be written by wrapping the formula with only a single `$`:

    This is inline: $\mathbf{y} = \mathbf{X}\boldsymbol\beta + \boldsymbol\varepsilon$

This is inline: $\mathbf{y} = \mathbf{X}\boldsymbol\beta + \boldsymbol\varepsilon$

Note that Markdown special characters need to be escaped with a backslash so they are treated as math rather than Markdown. For example, `*` and `_` become `\*` and `\_` respectively.

### Multiline equations

The standard LaTeX line break consisting of 2 backslashes needs to be replaced with 6 backslashes:

```TeX
$$f(k;p\_0^\*) = \begin{cases} p\_0^\* & \text{if }k=1, \\\\\\
1-p\_0^\* & \text {if }k=0.\end{cases}$$
```

$$f(k;p\_0^\*) = \begin{cases} p\_0^\* & \text{if }k=1, \\\\\\
1-p\_0^\* & \text {if }k=0.\end{cases}$$

### Publication abstracts

As Hugo and Academic attempt to parse TOML, Markdown, and LaTeX content in the abstract, the following guidelines should be followed just for the publication `abstract` and `abstract_short` fields:

- escape each LaTeX backslash (`\`) with an extra backslash, yielding `\\`
- escape each LaTeX underscore (`_`) with two backslashes, yielding `\\_`

Hence, `abstract = "${O(d_{\max})}$"` becomes `abstract = "${O(d\\_{\\max})}$"`.

## Table

Code:

```Markdown
| Command           | Description                    |
| ------------------| ------------------------------ |
| `hugo`            | Build your website.            |
| `hugo serve -w`   | View your website.             |
```

Result:

| Command           | Description                    |
| ------------------| ------------------------------ |
| `hugo`            | Build your website.            |
| `hugo serve -w`   | View your website.             |


## Alerts

Alerts are a useful feature that add side content such as tips, notes, or warnings to your articles. They are especially handy when writing educational tutorial-style articles. Use the corresponding shortcodes to enable alerts inside your content:

    {{%/* alert note */%}}
    Here's a tip or note...
    {{%/* /alert */%}}

This will display the following *note* block:

{{% alert note %}}
Here's a tip or note...
{{% /alert %}}

    {{%/* alert warning */%}}
    Here's some important information...
    {{%/* /alert */%}}

This will display the following *warning* block:

{{% alert warning %}}
Here's some important information...
{{% /alert %}}

## Table of Contents

A table of contents may be particularly useful for long posts or tutorial/documentation type content. Use the `{{%/* toc */%}}` shortcode anywhere you wish within your Markdown content to automatically generate a table of contents.
