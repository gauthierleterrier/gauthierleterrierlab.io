+++
title = "Introduction to abelian varieties"
date = 2018-11-15T11:00:41+01:00  # Schedule page publish date.
draft = false

# Talk start and end times.
#   End time can optionally be hidden by prefixing the line with `#`.
time_start =    2018-11-21T11:00:41+01:00
time_end =      2018-11-21T13:00:41+01:00

# Abstract and optional shortened version.
abstract = "Introduction to abelian varieties over fields : basic notions (as isogenies, dual abelian variety, etc.) were introduced."

# Name of event and optional event URL.
event = "Topics in algebraic geometry"
event_url = ""

# Location of event.
location = "EPFL, Lausanne, room [MA B2 485](https://plan.epfl.ch/?dim_floor=2&lang=fr&dim_lang=fr&tree_groups=centres_nevralgiques%2Cacces%2Cmobilite_reduite%2Censeignement%2Ccommerces_et_services%2Cvehicules%2Cinfrastructure_plan_grp&tree_group_layers_centres_nevralgiques=information_epfl%2Cguichet_etudiants&tree_group_layers_acces=metro&tree_group_layers_mobilite_reduite=&tree_group_layers_enseignement=&tree_group_layers_commerces_et_services=&tree_group_layers_vehicules=&tree_group_layers_infrastructure_plan_grp=batiments_query_plan&baselayer_ref=grp_backgrounds&map_x=533178&map_y=152428&map_zoom=14)."

# Is this a selected talk? (true/false)
selected = true

# Projects (optional).
#   Associate this talk with one or more of your projects.
#   Simply enter the filename (excluding '.md') of your project file in `content/project/`.
#   E.g. `projects = ["deep-learning"]` references `content/project/deep-learning.md`.
projects = []

# Tags (optional).
#   Set `tags = []` for no tags, or use the form `tags = ["A Tag", "Another Tag"]` for one or more tags.
tags = ["Abelian varieties"]

# Links (optional).
url_pdf = "/files/notes/Talk_EPFL_abelian_varieties_2018.pdf"
url_slides = ""
url_video = ""
url_code = ""

# Does the content use math formatting?
math = true

# Does the content use source code highlighting?
highlight = true

# Featured image
# Place your image in the `static/img/` folder and reference its filename below, e.g. `image = "example.jpg"`.
[header]
image = ""
caption = ""

+++
Abelian varieties give us a powerful tool to study general algebraic varieties. Typically, I will introduce the Albanese variety $\\mathrm{Alb}(X)$ attached to a smooth projective variety $X$ over a field $k$, which is some "universal" abelian variety associated with $X$.
