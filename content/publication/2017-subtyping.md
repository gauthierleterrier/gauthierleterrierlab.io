+++
title = "Test"
date = "2018-01-04"

# Authors. Comma separated list, e.g. `["Bob Smith", "David Jones"]`.
authors = ["Gauthier Leterrier"]

# Publication type.
# Legend:
# 0 = Uncategorized
# 1 = Conference proceedings
# 2 = Journal
# 3 = Work in progress
# 4 = Technical report
# 5 = Book
# 6 = Book chapter
publication_types = ["2"]

# Publication name and optional abbreviated version.
publication = "*Journal ???* 123-456"
publication_short = "In *???*"

# Abstract and optional shortened version.
# https://github.com/gcushen/hugo-academic/issues/57   or   http://zyblog.site/post/managing-content/#create-a-publication    for Latex in abstract
abstract = "One can use math symbols in the abstract : $\\mathbb F_p$..."
abstract_short = ""

# Featured image thumbnail (optional)
image_preview = ""

# Is this a selected publication? (true/false)
selected = true

# Projects (optional).
#   Associate this publication with one or more of your projects.
#   Simply enter the filename (excluding '.md') of your project file in `content/project/`.
#projects = ["data-integration"]

# Links (optional).
url_pdf = "files/papers/abc.pdf"
#url_preprint = "http://website.com"
#url_code = "#"
#url_dataset = "http://abc.xyz"
#url_project = "#"
#url_slides = "#"
#url_video = "#"
url_poster = "#"
#url_source = "http://def.xyz"

# Custom links (optional).
#   Uncomment line below to enable. For multiple links, use the form `[{...}, {...}, {...}]`.
url_custom = [{name = "BibTeX", url = "files/papers/Galoisorbits.bib"}]

# Does the content use math formatting?
math = true

# Does the content use source code highlighting?
highlight = true

# Featured image
# Place your image in the `static/img/` folder and reference its filename below, e.g. `image = "example.jpg"`.
#[header]
#image = "headers/bubbles-wide.jpg"
#caption = "My caption"

+++

More detail can easily be written here using *Markdown* and $\rm \LaTeX$ (e.g. $\mathbb F_q$) math code.