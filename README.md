# READ ME

(To write down... — I write here a few steps that I followed - moreless - to create my website. My knowledge being very poor in informatics, I very probably wrote some non-sense sentences in what follows).

1. Sign in to GitLab (or GitHub...) — avoid any dot "." in the username

2. For Mac, install Homebrew and Git (using Terminal), and create a SSH-RSA key ......

3. Create a project  USERNAME.gitlab.io  on gitlab. Ensure it is a public project (Settings -> General -> Permissions : public -> save). Create a file there, named  .gitlab-ci.yml, whose content is the template for HUGO websites (see my repository to copy/paste).

4. When you commit the file  .gitlab-ci.yml, go to Setting -> Pages. After a few minutes, a URL should be given to you. If you add your SSH key to the gitlab (in https://gitlab.com/profile/keys), this URL should get secured with a certificate.  This may take a few minutes at each step... (check the "CI/CD", represented by a rocket on the left menu bar)

5. Download the academic template (it uses "Hugo", itself based on "toml" and also HTML & CSS...) - see https://sourcethemes.com/academic/docs/install/.   The "ZIP" option was the easiest one for me (see my folders on this current gitlab project to see where the files are).

6. If you download these folders in   Users/USERNAME  (on a Mac computer), then you can add these files to gitlab via the following commands on Terminal :




    cd USERNAME.gitlab.io/
    
    git init
    
    git config --global user.name "USERNAME"
    
    git config --global user.email "your-email@example.xyz"
    
    git remote add origin https://gitlab.com/USERNAME/USERNAME.gitlab.io.git
    
    git add .
    
    git commit -m "Website initialisation"
    
    git push -u origin master




7. This should create the files in your gitlab project, and you should be able (after waiting 3-4 minutes) to access your website using the URL obtained above.

8. Modify the files   config.toml  using your data (see mine to know what to change, e.g. institution = "My university", interests = "Maths")

9. Enjoy :-)

10. ...





______________________________________________


1. To modify the copyright file :
   go in themes/academic/layouts/partials and then choose footer_section.html
   
   
2. To get URL in the about.md, see https://discourse.gohugo.io/t/difficulty-making-active-links/10452/3 :
   go in themes/academic/layouts/partials/widgets and then choose about.html to add "| markdownify" after ".institution"
   
   
3. To modify the icons in §contact, go to https://gitlab.com/gauthierleterrier/website3/blob/master/themes/academic/layouts/partials/widgets/contact.html  
and modify map-marker to map-marker-alt as given at https://fontawesome.com/icons?d=gallery&q=place&m=free


4. To modify the font of the header, I followed https://github.com/fischcheng/hugo-academic (and https://fischcheng.github.io/2016/09/14/automated-deployment-of-hugo-generated-site-to-github-pages/) : 
    1. add "custom.css" in a folder static/css (to be created) 
    2. in config.toml, write ["custom.css"]
    3. modify layouts/partials/head_custom.html as I did
    4. modify layouts/partials/header.html  as I did (i.e. adding one line at the bottom of the code)

5.  Changer la couleur d'un thème --> gauthierleterrier.gitlab.io/themes/academic/data/themescoffee.toml    et  changer les polices : https://gitlab.com/gauthierleterrier/gauthierleterrier.gitlab.io/blob/master/themes/academic/data/fonts/classic.toml

6.  Changer les widgets : https://gitlab.com/gauthierleterrier/gauthierleterrier.gitlab.io/blob/master/themes/academic/layouts/partials/widgets/about.html

7.  Changer le css : https://gitlab.com/gauthierleterrier/gauthierleterrier.gitlab.io/blob/master/themes/academic/layouts/partials/css/academic.css

8.  .

9.  .


